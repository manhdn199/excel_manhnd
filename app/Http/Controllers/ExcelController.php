<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\course;
use App\Models\dataUser;
use App\Models\student_has_course;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ExcelController extends Controller
{
    public const ADMIN = 1;
    public const USER = 2;
    /**
     * @param $worksheet
     * @param $col
     * @param $row
     * @return mixed
     */
    public function checkValue($worksheet, $col, $row){
        return $worksheet->getActiveSheet()->getCell($col . $row)
            ->getValue();
    }

    /**
     * @throws Exception
     */
    public function readExcel(): void
    {
        $inputFileName = '../resources/views/test_btob1.xlsx';

        $reader = new Xlsx();

        $reader->setLoadSheetsOnly("登録フォーム(模擬試験)");
        $spreadsheet = $reader->load($inputFileName);

        $worksheet = $spreadsheet->getActiveSheet();

        $highestRow = $worksheet->getHighestRow(); // e.g. 10d
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumn++;
        $dataStudents = [];
//        dd($this->checkArrayCount($highestRow,$highestColumn,$worksheet));
        for ($row = 12 ; $row <= $this->MaxStudent($highestRow, $worksheet); $row++){

            $dataStudents[$row] = [
                'name_student' => $this->checkValue($worksheet, 'B', $row),
                'rand_number' => $this->checkValue($worksheet, 'D', $row),
                'first_name' => $this->checkValue($worksheet, 'E', $row),
                'last_name' => $this->checkValue($worksheet, 'F', $row),
                'first_name_furigana' => $this->checkValue($worksheet, 'G', $row),
                'last_name_furigana' => $this->checkValue($worksheet, 'H', $row),
                'email' => $this->checkValue($worksheet, 'I', $row),
            ];

            for ($col = 'J'; $col !== $highestColumn; ++$col){
                    $dataRegister = [
                        'name_course' => $this->checkValue($worksheet, $col, 4) ?: '',
                        'info' => $this->checkValue($worksheet, $col, $row) ?: '',
                    ];

                if(!empty($this->checkValue($worksheet, $col, 4))
                    && !empty($this->checkValue($worksheet, $col, 5))
                    && empty($this->checkValue($worksheet, $col, 6))
                ) {
                    $dataRegister = [
                        'name_course' => $this->checkValue($worksheet, $col, 4) ?: '',
                        'exam_place' => $this->checkValue($worksheet, $col, 5) ?: '',
                        'info' => $this->checkValue($worksheet, $col, $row) ?: '',
                    ];
                }

                    $dataStudents[$row]['course'][] = $dataRegister;

            }
        }
//        echo "<pre>";
//
//        print_r(($dataStudents));

        // data học viên đăng kí khóa học theo từng khóa - chưa hoàn thiện
        $dataCourse = [];
        $dem = 0;
        $check = 3;
        $checkCourse = 4;

        foreach (($dataStudents[12]['course']) as $k => $value){

                if ($value['name_course'] !== '' && !isset($value['exam_place'])){
//                    $dataCourse[] = $value['name_course'];

                    for ($i = $dem; $i < $checkCourse; $i++){
                        $dem++;
                        $dataCourse[$i][$i] = $dataStudents[12]['course'][$i]['info'];
                    }
                    $checkCourse += 4;

                } elseif (!empty($value['name_course'])){
                    $dataCourse[$check] = [];

                    $dataCourse[$check]['name_course'] = $value['name_course'];

                    for ($i = $dem; $i < $check; $i++){
                        $dem++;
                        $dataCourse[$check][$i][] = $dataStudents[12]['course'][20]['info'];
                    }
                    $check += 3;

                }
        }

        echo "<pre>";

        print_r($dataCourse);

        // đổ ra tất cả data
//        echo "<pre>";
//        print_r($dataStudents);
//        echo '<table>' . "\n";
//        for ($row = 1; $row <= $highestRow; ++$row) {
//            echo '<tr>' . PHP_EOL;
//            for ($col = 'A'; $col !== $highestColumn; ++$col) {
//
//                $checkValue = $this->checkValue($worksheet, $col, $row);
//
//                echo '<td>' ;
//
//                if($checkValue === '○'){
//                    echo '○' . $col . $row;
//                } else{
//                    echo $checkValue . $col . $row ;
//                }
//
//                echo '</td>' . PHP_EOL;
//            }
//            echo '</tr>' . PHP_EOL;
//        }
//        echo '</table>' . PHP_EOL;

    }

    /**
     * @param $highestRow
     * @param $highestColumn
     * @param $worksheet
     * @param $stringCheck
     * @param array $valueCheckFunction
     * @return array
     */
    public function checkArrayCount($highestRow, $highestColumn, $worksheet, $stringCheck = "=COUNTIF($", $valueCheckFunction = []): array
    {
        for ($row = 1; $row <= $highestRow; ++$row) {
            for ($col = 'A'; $col !== $highestColumn; ++$col) {

                $checkValue = $this->checkValue($worksheet, $col, $row);

                if (strpos($checkValue, $stringCheck) !== false) {
                    $valueCheckFunction[] = [
                        $checkValue,
                        $col,
                        $row,];
                }
            }
        }

        return $valueCheckFunction;
    }

    /**
     * @param $highestRow
     * @param $worksheet
     * @return mixed
     */
    public function MaxStudent($highestRow, $worksheet): array
    {
        $arrayA = [];
        $highRow = $worksheet->getActiveSheet()->getHighestRow(); // e.g. 10d

        for ($row = 1; $row <= $highRow; ++$row) {
            if ($this->checkValue($worksheet, 'B', $row) === '受講者'){
                $startStudent = $row + 1;
                break;
            }
        }

        for ($row = $startStudent; $row <= $highestRow; ++$row) {

            $checkValue = $this->checkValue($worksheet, 'A', $row);

            if(empty($checkValue))
            {
                break;
            }

            $arrayA[] = $checkValue;
        }

        return [
            'startStudents' => $startStudent,
            'endStudents' => max($arrayA) + 11];
    }

    public function MaxUserAdmin($highestRow, $worksheet): int
    {
        for ($row = 8; $row <= $highestRow; ++$row) {

            $checkValue = $this->checkValue($worksheet, 'B', $row);

            if($checkValue === '受講者' && !is_int($checkValue))
            {
                break;
            }

        }

        return $row - 1;
    }

    public function findColumn($worksheet, $highestCol): array
    {
        $arrayCheck = [
            '情報',
            '疾病',
            '総論',
        ];

        $arrayColumns = [];
        for ($col = 'J'; $col !== $highestCol; ++$col) {
            $check = $this->checkValue($worksheet, $col, 5);

            if (!empty($check) && !in_array($check, $arrayCheck, true)){
                $arrayColumns[] = $col;
            }
        }

        return $arrayColumns;
    }

    public function importDb(): bool
    {
        $worksheet = $this->connectExcel();
        $highestRow = $worksheet->getActiveSheet()->getHighestRow(); // e.g. 10d
        $highestColumn = $worksheet->getActiveSheet()->getHighestColumn();
        $highestColumn++;
        $arrStartEndStudent = $this->MaxStudent($highestRow, $worksheet);

        $company = Company::create([
            'name' => $this->checkValue($worksheet,'D', 2),
        ]);

        $maxUserAdmin =  $this->MaxUserAdmin($highestRow, $worksheet);

        for ($row = 8; $row <= $maxUserAdmin; ++$row) {
            $dataCreateAdmin = dataUser::create([
                'code' => $this->checkValue($worksheet,'D', $row),
                'first_name' => $this->checkValue($worksheet,'E', $row),
                'last_name' => $this->checkValue($worksheet,'F', $row),
                'first_name_furi' => $this->checkValue($worksheet,'G', $row),
                'last_name_furi' => $this->checkValue($worksheet,'H', $row),
                'email' => $this->checkValue($worksheet,'I', $row),
                'id_company' => $company->id,
                'role' => self::ADMIN,
                'location' => $row,
            ]);
            if (empty($dataCreateAdmin->code)
                && empty($dataCreateAdmin->first_name)
                && empty($dataCreateAdmin->last_name)
                && empty($dataCreateAdmin->first_name_furi)
                && empty($dataCreateAdmin->last_name_furi)
                && empty($dataCreateAdmin->email)
            ) {
                dataUser::find($dataCreateAdmin->id)->delete();
            }
        }

        for ($row = $arrStartEndStudent['startStudents']; $row <= $arrStartEndStudent['endStudents']; $row++){
            $dataStudents = [
                'code' => $this->checkValue($worksheet,'D', $row),
                'first_name' => $this->checkValue($worksheet,'E', $row),
                'last_name' => $this->checkValue($worksheet,'F', $row),
                'first_name_furi' => $this->checkValue($worksheet,'G', $row),
                'last_name_furi' => $this->checkValue($worksheet,'H', $row),
                'email' => $this->checkValue($worksheet,'I', $row),
                'id_company' => $company->id,
                'role' => self::USER,
                'location' => $row,
            ];
            $createStudent = dataUser::create($dataStudents);
            if (empty($createStudent->code)
                && empty($createStudent->first_name)
                && empty($createStudent->last_name)
                && empty($createStudent->first_name_furi)
                && empty($createStudent->last_name_furi)
                && empty($createStudent->email)
            ) {
                dataUser::find($createStudent->id)->delete();
            } else{
                $idStudentCreate[$createStudent->id]['id'] = $createStudent->id;
                $idStudentCreate[$createStudent->id]['location'] = $createStudent->location;
            }

        }

        $arrayCheck = [
            '情報',
            '疾病',
            '総論',
        ];

        for ($col = 'J'; $col !== $highestColumn; ++$col) {
            $check = $this->checkValue($worksheet, $col,4);
            $checkDataType = $this->checkValue($worksheet, $col,5);

            if (!empty($check)){
                if (!is_string($check)){
                    $arrayNameCourse[$col]['name'] = $check->getPlainText();

                } else{
                    $arrayNameCourse[$col]['name'] = $check;
                }

                if (!in_array($checkDataType, $arrayCheck, true)){
                    $count = 4;
                } else{
                    $count = 3;
                }

                for ($i = $col; $i !== $highestColumn; ++$i){
                    if ($count === 0){
                        break;
                    }

                    switch ($count){
                        case 4: {
                            $arrayNameCourse[$col]['dataType'][] =$checkDataType;
                            $arrayNameCourse[$col]['dataType'][] = $i;

                            break;
                        }
                        case 3: {
                            $arrayNameCourse[$col]['dataType'][] = '情報';
                            $arrayNameCourse[$col]['dataType'][] = $i;

                            break;
                        }
                        case 2: {
                            $arrayNameCourse[$col]['dataType'][] ='疾病';
                            $arrayNameCourse[$col]['dataType'][] = $i;

                            break;
                        }
                        case 1: {
                            $arrayNameCourse[$col]['dataType'][] ='総論';
                            $arrayNameCourse[$col]['dataType'][] = $i;

                            break;
                        }
                    }

                    $count--;
                }
            }
        }
        foreach ($arrayNameCourse as $key =>  $value){
            $createCourse = course::create([
                'name' => $value['name'],
                'id_company' => $company->id,
                'location' => $key,
            ]);

            $idCourseCreate[$createCourse->id]['id'] = $createCourse->id;
            $idCourseCreate[$createCourse->id]['name'] = $createCourse->name;
            $idCourseCreate[$createCourse->id]['location'] = $createCourse->location;
        }
        foreach ($idStudentCreate as $valueStudent){
            foreach ($idCourseCreate as $valueCourse){
//                $check = $this->checkValue($worksheet, $valueCourse['location'], 4);
                if (count($arrayNameCourse[$valueCourse['location']]['dataType']) == 6){
                    $data = [
                        'infor' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][1], $valueStudent['location'])) ? 1 : 2,
                        'status' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][3], $valueStudent['location'])) ? 1 : 2,
                        'general_comment' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][5], $valueStudent['location'])) ? 1 : 2,
                        'exam_value' => '',
                        'name_exam_value' => '',
                        'location' => $valueStudent['location']
                    ];
                } elseif(count($arrayNameCourse[$valueCourse['location']]['dataType']) == 8){
                    $data = [
                        'infor' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][3], $valueStudent['location'])) ? 1 : 2,
                        'status' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][5], $valueStudent['location'])) ? 1 : 2,
                        'general_comment' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][7], $valueStudent['location'])) ? 1 : 2,
                        'exam_value' => !empty($this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][1], $valueStudent['location'])) ? $this->checkValue($worksheet, $arrayNameCourse[$valueCourse['location']]['dataType'][1], $valueStudent['location']) : '',
                        'name_exam_value' => $arrayNameCourse[$valueCourse['location']]['dataType'][1],
                        'location' => $valueStudent['location']
                        ];
                }

                $this->addStudentToCourse($valueStudent['id'], $valueCourse['id'], $company->id, $data);

            }
        }
        return true;
    }

    public function updateExcel(){
        $worksheet = $this->connectExcel();
        $highestRow = $worksheet->getActiveSheet()->getHighestRow(); // e.g. 10d
        $count = 1;

        for ($row = 12 ; $row <= $this->MaxStudent($highestRow, $worksheet); $row++){

            $dataStudents = [
                'name' => $this->checkValue($worksheet, 'E', $row),
                'last_name' => $this->checkValue($worksheet, 'F', $row),
            ];

            dataUser::where('id', '=', $count)
                ->update($dataStudents);

            $count++;
        }

        return true;
    }

    public function connectExcel()
    {
        $inputFileName = '../resources/views/test_btob1.xlsx';
//        $inputFileName = '../resources/views/test_btob2.xlsx';

        $reader = new Xlsx();

        $reader->setLoadSheetsOnly("登録フォーム(模擬試験)");

        try {
            return $reader->load($inputFileName);
        } catch (Exception $e) {
        }

        return false;

    }

    public function checkValueUpdate(){
        $worksheet = $this->connectExcel();
        $highestRow = $worksheet->getActiveSheet()->getHighestRow(); // e.g. 10d
        $highestColumn = $worksheet->getActiveSheet()->getHighestColumn(); // e.g 'F'
        $highestColumn++;
        $maxUserAdmin =  $this->MaxUserAdmin($highestRow, $worksheet);
        $id_company = 72;
        if (empty($this->checkValue($worksheet,'D',2)) || $this->checkNameCompany($worksheet)){
            $this->changerColor('D' . 2, $worksheet);
        }

        for ($row = 8; $row <= $maxUserAdmin; ++$row) {
            for ($col = 'D'; $col !== 'J'; ++$col) {
                $check = $this->checkValue($worksheet,$col,$row);

                $this->checkDataUser($check,$col,'D','code',$row, $worksheet);
                $this->checkDataUser($check,$col,'E','first_name',$row, $worksheet);
                $this->checkDataUser($check,$col,'F','last_name',$row, $worksheet);
                $this->checkDataUser($check,$col,'G','first_name_furi',$row, $worksheet);
                $this->checkDataUser($check,$col,'H','last_name_furi',$row, $worksheet);
                $this->checkDataUser($check,$col,'I','email',$row, $worksheet);
            }
        }

        $arrStartEndStudent = $this->MaxStudent($highestRow, $worksheet);
        $arrColumns = $this->findColumn($worksheet, $highestColumn);
        $maxLocationUser = $this->getMaxLocationUser($id_company);

        for ($row = $arrStartEndStudent['startStudents']; $row <= $maxLocationUser; ++$row) {
            for ($col = 'D'; $col !== 'J'; ++$col) {
                $check = $this->checkValue($worksheet,$col,$row);

                $this->checkDataUser($check,$col,'D','code',$row, $worksheet);
                $this->checkDataUser($check,$col,'E','first_name',$row, $worksheet);
                $this->checkDataUser($check,$col,'F','last_name',$row, $worksheet);
                $this->checkDataUser($check,$col,'G','first_name_furi',$row, $worksheet);
                $this->checkDataUser($check,$col,'H','last_name_furi',$row, $worksheet);
                $this->checkDataUser($check,$col,'I','email',$row, $worksheet);
            }
        }

        for ($row = $arrStartEndStudent['startStudents']; $row <= $maxLocationUser; ++$row)
        {
            foreach ($arrColumns as $key => $value){
                $check = $this->checkValue($worksheet,$value,$row);
                $valueExam = $this->getNameExam_venue($value, $row);

                if (empty($valueExam->exam_venue) || $check !== $valueExam->exam_venue) {
                    $this->changerColor($value . $row, $worksheet);
                }

            }

        }

        $file_name = "Check change_" . date('Y:m:d').'.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$file_name");
        header('Cache-Control: max-age=0');

        try {
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($worksheet, 'Xls');
        } catch (\PhpOffice\PhpSpreadsheet\Writer\Exception $e) {
        }

        $writer->save('php://output');
//        $writer = IOFactory::createWriter($worksheet, 'Xlsx');
////        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
////        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
//        $writer->save("../resources/views/excel.xlsx");
    }

    public function checkDataUser($check,$col,$COLV,$field,$row, $worksheet): void
    {
        $data = DB::table('dataUser')->select(
            'code',
            'first_name',
            'last_name',
            'first_name_furi',
            'last_name_furi',
            'email',
            'role',
        )->where('location', '=', $row)->first();

        if ($col === $COLV) {
            if (empty($check) || ($data->$field !== $check)) {
                $this->changerColor($col . $row, $worksheet);
            }
        }
    }

    public function changerColor($string, $worksheet): void
    {
        $worksheet->getActiveSheet()->getStyle($string)->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFF0000');
    }

    public function addStudentToCourse($id_student, $id_course,$id_company,  $data = []): void
    {

         student_has_course::create([
            'id_student' => $id_student,
            'id_course' => $id_course,
            'id_company' => $id_company,
            'infor' => $data['infor'],
            'status' => $data['status'],
            'general_comment' => $data['general_comment'],
            'exam_venue' => $data['exam_value'],
            'name_exam_venue' => $data['name_exam_value'],
            'location' => $data['location']
            ]
        );
    }

    public function getIdExam_venue($id_company){
        $arrayRow = DB::table('student_has_course')->select('location')
            ->where('id_company', '=', $id_company)
            ->where('location', 'not REGEXP', '^[0-9]+')->distinct()->get()->toArray();


        return $arrayRow;
    }

    public function checkNameCompany($worksheet){
        $nameCompanyExcel = $this->checkValue($worksheet,'D',2);

        $check = Company::select('id')
            ->where('name', 'like', $nameCompanyExcel)->get()->toArray();

        if (empty($check)){
            return true;
        }

        return false;
    }

    public function getNameExam_venue($string, $int)
    {
        return student_has_course::select('exam_venue')
            ->where('name_exam_venue', '=', (string)$string)
            ->where('location', '=', $int)
            ->first();
    }

    public function getMaxLocationUser($id_company){
         return DB::table('dataUser')->where('id_company', '=',$id_company)->max('location');
    }
}
