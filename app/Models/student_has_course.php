<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class student_has_course extends Model
{
    use HasFactory;

    protected $table = 'student_has_course';

    protected $fillable = [
        'id_student',
        'id_course',
        'infor',
        'status',
        'general_comment',
        'exam_venue',
        'name_exam_venue',
        'location',
        'id_company'
    ];
}
