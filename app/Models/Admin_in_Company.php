<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin_in_Company extends Model
{
    use HasFactory;

    protected $table = 'Admin_in_Company';

    protected $fillable = [
        'code',
        'first_name',
        'last_name',
        'first_name_furi',
        'last_name_furi',
        'email',
        'id_company',
        'role'
    ];
}
