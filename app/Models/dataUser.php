<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dataUser extends Model
{
    use HasFactory;

    protected $table = 'dataUser';

    protected $fillable = [
        'code',
        'first_name',
        'last_name',
        'first_name_furi',
        'last_name_furi',
        'email',
        'id_company',
        'role',
        'location'
    ];
}
