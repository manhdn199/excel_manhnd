<html>
<body>
<span id="'message"></span>
<form action="" method="post" id="load_excel_form" enctype="multipart/form-data">
    <table class="table">
        <tr>
            <td>select Excel</td>
            <td>
                <input type="file" name="select_excel">
            </td>
            <td>
                <input type="submit" name="load">
            </td>
        </tr>
    </table>
</form>
<div id="excel_area"></div>
</body>
<script>
    $(document).ready(function (){
        $('#load_excel_form').on('submit', function (event){
            event.preventDefault();
            $.ajax({
                url:"upload.php",
                method:"POST",
                data: new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function (data){
                    $('#excel_area').html(data);
                }
            })
        })
    })
</script>
</html>
