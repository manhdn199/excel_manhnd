<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';
Route::get('excel',[\App\Http\Controllers\ExcelController::class,'readExcel']);
Route::get('import',[\App\Http\Controllers\ExcelController::class,'importDb']);
Route::get('update',[\App\Http\Controllers\ExcelController::class,'updateExcel']);
Route::get('check',[\App\Http\Controllers\ExcelController::class,'checkValueUpdate']);
Route::get('haha',[\App\Http\Controllers\ExcelController::class,'getIdExam_venue']);
